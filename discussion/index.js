/* 
log in the console the following GWA of a student per sudject

98.5
94.3
89.2
90.0

*/

const grades = [98.5, 94.3, 89.2, 90.0]
console.log(grades)
console.log(grades[3])

// Objects
//  Collection of related data and functionalities; usually representing real world objects

let grade = {
    // curly braces = initializer for creating objects
    // key-value pair

    /* 
        key - field/identifier/property to describe the data/value;
        value - information that serves as the value of the key.field
    */
    math: 98.5,
    english: 94.3,
    science: 90.0,
    MAPEH: 89.2,    
}

console.log(grade);

/* 
    Accessing Elements Inside the Object

        Syntax for Creating an Object
            
            let objectName = {
                keyA: valueA,
                keyB: valueB,
                ...
            }

        Dot Notation - used to access the specific property and value of the object; preferred in terms of object access
*/

console.log(grade.english);
// Accessing two keys in the object
console.log(grade.MAPEH, grade.science);

/* 
create a cellphone object that has the following keys (supply your own values for each)
        brand
        color
        mfd

*/

let cellphone = {
    brand : 'samsung',
    color : 'gray',
    mfd : 2020,
}

console.log(typeof cellphone);

let student = {
    firstName: 'John',
    lastName: 'Smith',
    mobileNo: 090239432,
    location:{
        city:'tokyo',
        country: 'Japan',
    },
    email:['jon@mail.com', 'johns@mail.com'],

    fullName:(
        function name(fullName){
            // this - referes to the object  where it is inserted.
            /* 
                in this case, the code below could be typed as 
                    console.log(student.firstName + ' ' + student.lastName)
                
            */
        console.log(this.firstName + ' ' + this.lastName)
    })
};

// console.log(student.firstName + ' ' + student.lastName)
student.fullName()
console.log(student.location.city)
console.log(student.email[1])

/* 
    Array of Objects
*/

let contactList = [
    {
        firstName: 'John',
        lastName: 'Smith',
        location: 'Japan',
        
    },
    {
        firstName: 'Jane',
        lastName: 'Smith',
        location: 'Japan',
    },
    {
        firstName: 'Jasmine',
        lastName: 'Smith',
        location: 'Japan',
    }
];

// Accessing an element inside the array

console.log(contactList[1]); // {firstName: 'Jane', lastName: 'Smith', location: 'Japan'}firstName: "Jane"lastName: "Smith"location: "Japan"[[Prototype]]: Object

// accessing a ket in an object that is inside the array.

console.log(contactList[2].firstName) //Jasmine

// Constructor Function - create a reusable function to create several objects

/* 
    SYNTAX

        function objectName (keyA, keyB){
            this.keyA = keyA,
            this,keyB = keyB
        }
*/
/* 
    > useful for creating copies/instances of an object

    instance - concrete occurence of any object which emphasizes on the unique identity of it.
    

    object literals 
        
        let objectName = {}

*/


// Reusable Function/Object Method
function laptop (name, manufacturedDate){
    this.name = name,
    this.manufacturedDate = manufacturedDate
};
/* 
it also performs different commands
console.log(this)
*/
// new keyworkd refers/signifies that there will be a new object under the function (laptop)

let laptop1 = new laptop('lenovo', 2008);
console.log(laptop1);

let laptop2 = new laptop('toshiba', 1997);
console.log(laptop2);


/* 
create a variable and store the laptop1 and laptop2 in it (the resulting output must be an array)
*/

let laptop_total = [laptop1, laptop2];
console.log(laptop_total)

/* 
    Initializing, adding, deleting, reassigning  of object properties
*/

let car = {};
console.log(car);
// adding object properties

car.name = 'honda civic'
console.log(car);

car['manufacturedDate'] = 2020;
console.log(car);

// reassigning object (same/existing properties, but assigning a different value)

car.name = 'volvo';
console.log(car);

// deleting object properties

delete car.manufacturedDate;
console.log(car);

let person = { // put parentheses ()
    name: 'Jherson',
    talk: function(){
        console.log('my name is ' + this.name);
    }
};

// add property
person.walk = function(){
    console.log(this.name + " walked 25 steps forward")
}



let friend = {
    firstName: 'Joe',
    lastName: 'Doe',
    address:{
        city: 'Austin',
        state: 'Texas',
    },
    email:['joe@mail.com', 'joedoe@mail.com'],
    introduce:function(){
        console.log('hello! my name is ' + this.firstName + ' ' + this.lastName)
    }
};

/* let pokemon = {
    name : 'Pikachu',
    level : 3,
    health : 100,
    attack : 50,
    tackle: function(){
        console.log('this pokemon tackled target pokemon')
        console.log("target pokemon's health is now reduced to target pokemonHealth")
    }
}

function Pokemon(name, level){
    this.name = name,
    this.level = level,
    this.health = 2*level,
    this.attack = level
} 
 */
let pokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function (){
		console.log("This pokemon tackeld targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}



let pikachu = new Pokemon ('pikachu', 16);
let charizard = new Pokemon ('charizard', 8);
let bulbasaur = new Pokemon ('bulbasaur', 9);
let raichu = new Pokemon('raichu', 16);




































